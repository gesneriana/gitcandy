﻿PRAGMA foreign_keys = OFF;

DROP TABLE IF EXISTS "main"."AuthorizationLog";
CREATE TABLE "AuthorizationLog" (
"AuthCode"  GUID NOT NULL,
"UserID"  INTEGER NOT NULL,
"IssueDate"  Datetime NOT NULL,
"Expires"  Datetime NOT NULL,
"IssueIp"  varchar(40) NOT NULL,
"LastIp"  varchar(40) NOT NULL,
"IsValid"  Bit NOT NULL,
PRIMARY KEY ("AuthCode"),
CONSTRAINT "UserID" FOREIGN KEY ("UserID") REFERENCES "Users" ("ID")
);

INSERT INTO "main"."AuthorizationLog" VALUES ('"??*m?aN?K?
P?', 1, '2017-05-25 21:44:04.2504993', '2017-06-09 21:44:04.4535109', '::1', '::1', 1);

DROP TABLE IF EXISTS "main"."Repositories";
CREATE TABLE "Repositories" (
"ID"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"Name"  nvarchar(50) NOT NULL COLLATE NOCASE ,
"Description"  nvarchar(500) NOT NULL,
"CreationDate"  datetime NOT NULL,
"IsPrivate"  Bit NOT NULL,
"AllowAnonymousRead"  Bit NOT NULL,
"AllowAnonymousWrite"  Bit NOT NULL
);

INSERT INTO "main"."Repositories" VALUES (1, 'Test', '����', '2017-05-25 13:44:38.5554614Z', 0, 1, 0);

DROP TABLE IF EXISTS "main"."sqlite_sequence";
CREATE TABLE sqlite_sequence(name,seq);

INSERT INTO "main"."sqlite_sequence" VALUES ('Users', 1);
INSERT INTO "main"."sqlite_sequence" VALUES ('Repositories', 1);

DROP TABLE IF EXISTS "main"."SshKeys";
CREATE TABLE "SshKeys" (
"ID"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"UserID"  INTEGER NOT NULL,
"KeyType"  varchar(20) NOT NULL,
"Fingerprint"  Char(47) NOT NULL,
"PublicKey"  varchar(600) NOT NULL,
"ImportData"  datetime NOT NULL,
"LastUse"  datetime NOT NULL,
CONSTRAINT "UserID" FOREIGN KEY ("UserID") REFERENCES "Users" ("ID")
);

DROP TABLE IF EXISTS "main"."TeamRepositoryRole";
CREATE TABLE "TeamRepositoryRole" (
"TeamID"  INTEGER NOT NULL,
"RepositoryID"  INTEGER NOT NULL,
"AllowRead"  Bit NOT NULL,
"AllowWrite"  Bit NOT NULL,
CONSTRAINT "TeamID" FOREIGN KEY ("TeamID") REFERENCES "Teams" ("ID"),
CONSTRAINT "RepositoryID" FOREIGN KEY ("RepositoryID") REFERENCES "Repositories" ("ID")
);

DROP TABLE IF EXISTS "main"."Teams";
CREATE TABLE "Teams" (
"ID"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"Name"  nvarchar(20) NOT NULL,
"Description"  nvarchar(500) NOT NULL,
"CreationDate"  datetime NOT NULL
);

DROP TABLE IF EXISTS "main"."UserRepositoryRole";
CREATE TABLE "UserRepositoryRole" (
"UserID"  INTEGER NOT NULL,
"RepositoryID"  INTEGER NOT NULL,
"AllowRead"  Bit NOT NULL,
"AllowWrite"  Bit NOT NULL,
"IsOwner"  Bit NOT NULL,
CONSTRAINT "UserID" FOREIGN KEY ("UserID") REFERENCES "Users" ("ID"),
CONSTRAINT "RepositoryID" FOREIGN KEY ("RepositoryID") REFERENCES "Repositories" ("ID")
);

INSERT INTO "main"."UserRepositoryRole" VALUES (1, 1, 1, 1, 1);

DROP TABLE IF EXISTS "main"."Users";
CREATE TABLE "Users" (
"ID"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
"Name"  nvarchar(20) NOT NULL,
"Nickname"  nvarchar(20) NOT NULL,
"Email"  nvarchar(50) NOT NULL,
"PasswordVersion"  INTEGER NOT NULL,
"Password"  char(32) NOT NULL,
"Description"  nvarchar(500) NOT NULL,
"IsSystemAdministrator"  Bit NOT NULL,
"CreationDate"  Datetime NOT NULL
);

INSERT INTO "main"."Users" VALUES (1, 'admin', 'admin', 's694060865@163.com', 2, '0428E27ECFCC5A98D41488C118DB5719', 'GitCandy����Ա', 1, '2017-05-25 13:44:03.5784609Z');

DROP TABLE IF EXISTS "main"."UserTeamRole";
CREATE TABLE "UserTeamRole" (
"UserID"  INTEGER NOT NULL,
"TeamID"  INTEGER NOT NULL,
"IsAdministrator"  Bit NOT NULL,
CONSTRAINT "UserID" FOREIGN KEY ("UserID") REFERENCES "Users" ("ID"),
CONSTRAINT "TeamID" FOREIGN KEY ("TeamID") REFERENCES "Teams" ("ID")
);

CREATE UNIQUE INDEX "main"."Repositories_IX_Repository_Name"
ON "Repositories" ("Name" ASC);

CREATE UNIQUE INDEX "main"."UNQ_Team_Repository"
ON "TeamRepositoryRole" ("TeamID" ASC, "RepositoryID" ASC);

CREATE UNIQUE INDEX "main"."Teams_IX_Team_Name"
ON "Teams" ("Name" ASC);

CREATE UNIQUE INDEX "main"."UNQ_User_Repository"
ON "UserRepositoryRole" ("UserID" ASC, "RepositoryID" ASC);

CREATE UNIQUE INDEX "main"."Users_IX_User_Email"
ON "Users" ("Name" ASC);
CREATE UNIQUE INDEX "main"."Users_IX_User_Name"
ON "Users" ("Email" ASC);

CREATE UNIQUE INDEX "main"."UNQ_User_Team"
ON "UserTeamRole" ("UserID" ASC, "TeamID" ASC);
